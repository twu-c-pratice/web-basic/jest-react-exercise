import React from 'react';

import '@testing-library/jest-dom/extend-expect';
import '@testing-library/react/cleanup-after-each';

import { fireEvent, render } from '@testing-library/react';

import Switcher from '../Switcher';

test('Switcher组件通过点击按钮显示开／关', () => {
  // <--start
  // TODO 4: 给出正确的测试
  const { getByTestId } = render(<Switcher />);
  const switcher = getByTestId('switch-button');

  expect(switcher).toHaveTextContent('开');

  fireEvent.click(switcher);
  expect(switcher).toHaveTextContent('关');

  fireEvent.click(switcher);
  expect(switcher).toHaveTextContent('开');
  // --end->
});
